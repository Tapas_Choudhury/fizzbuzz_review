﻿using FizzBuzz.BL.Contacts;
using FizzBuzz.DAL.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.BL.Implementation
{
    public class SetFizzBuzz : FizzBuzzContract
    {
        public override string IsValid(int number)
        {

            if (number % 15 == 0)
            {
                FizzBuzzDAL fizzBuzzDal = new FizzBuzzDAL();
                return DateTime.Now.DayOfWeek.Equals(DayOfWeek.Wednesday) ? fizzBuzzDal.WizzTemplate + fizzBuzzDal.WuzzTemplate : fizzBuzzDal.FizzTemplate + fizzBuzzDal.BuzzTemplate;                
            }
            return Next.IsValid(number);
        }


    }
}
