﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.BL.Contacts
{
    public abstract class FizzBuzzContract
    {
        public FizzBuzzContract SetNext(FizzBuzzContract next)
        {
            Next = next;
            return Next;
        }

        protected FizzBuzzContract Next { get; private set; }

        public abstract string IsValid(int number);
    }
}
