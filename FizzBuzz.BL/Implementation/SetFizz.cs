﻿using FizzBuzz.BL.Contacts;
using FizzBuzz.DAL.Contracts;
using FizzBuzz.DAL.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.BL.Implementation
{
    public class SetFizz : FizzBuzzContract
    {
        public override string IsValid(int number)
        {

            if (number % 3 == 0)
            {   
                FizzBuzzDAL fizzBuzzDal = new FizzBuzzDAL();
                return DateTime.Now.DayOfWeek.Equals(DayOfWeek.Wednesday) ? fizzBuzzDal.WizzTemplate : fizzBuzzDal.FizzTemplate;                
            }
            return Next.IsValid(number);
        }


    }
}
