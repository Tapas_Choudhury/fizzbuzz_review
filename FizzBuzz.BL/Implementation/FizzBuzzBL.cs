﻿using System;
using System.Collections.Generic;
using System.Drawing;
using FizzBuzz.BL.Contacts;
using FizzBuzz.DAL.Contracts;
using FizzBuzz.DAL.Domain;

namespace FizzBuzz.BL.Implementation
{
    enum Rule
    {
        Fizz, Buzz
    }
    
    public class FizzBuzzBL : IFizzBuzzBL
    {
        public IFizzBuzzDAL FizzBuzzData { get; set; }
        public int MaxNumber { get; set; }        
        public List<FizzBuzzDomain> FizzBuzzNumbers { get; set; }

        public FizzBuzzBL(IFizzBuzzDAL data)
        {
            FizzBuzzData = data;            
            FizzBuzzNumbers = new List<FizzBuzzDomain>();
        }

        public void GetFizzBuzzNumbers()
        {
           

            for (int i = 1; i <= MaxNumber; i++)
            {
                FizzBuzzDomain businessObj = new FizzBuzzDomain();
                businessObj.Number = i.ToString();
                GetFizzBuzz fizzBuzz = new GetFizzBuzz();
                businessObj.DisplayText = fizzBuzz.DisplayNumner(i);               
                FizzBuzzNumbers.Add(businessObj);
            }
        }       
    }
}
