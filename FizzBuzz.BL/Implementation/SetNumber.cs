﻿using FizzBuzz.BL.Contacts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.BL.Implementation
{
    public class SetNumber : FizzBuzzContract
    {
        public override string IsValid(int number)
        {
            return number.ToString(CultureInfo.InvariantCulture);
        }
    }
}
