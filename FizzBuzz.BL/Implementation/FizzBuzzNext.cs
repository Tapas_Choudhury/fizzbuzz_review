﻿using FizzBuzz.BL.Contacts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.BL.Implementation
{
    public class FizzBuzzNext
    {
        private FizzBuzzNext()
        {
            Head = new SetFizzBuzz();
            Head.SetNext(new SetBuzz())
                .SetNext(new SetFizz())
                .SetNext(new SetNumber());
        }
        private FizzBuzzContract Head { get; set; }

        public static string DisplayNumber(int number)
        {
            return _instance.Head.IsValid(number);
        }

        private static readonly FizzBuzzNext _instance = new FizzBuzzNext();
    }
}
