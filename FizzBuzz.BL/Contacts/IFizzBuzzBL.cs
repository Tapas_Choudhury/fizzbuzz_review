﻿using System;
using System.Collections.Generic;

using FizzBuzz.DAL.Domain;
using FizzBuzz.DAL.Contracts;

namespace FizzBuzz.BL.Contacts
{
    public interface IFizzBuzzBL
    {
        IFizzBuzzDAL FizzBuzzData { get; set; }
        int MaxNumber { get; set; }
       // Dictionary<String, int> RuleSet { get; set; }
        List<FizzBuzzDomain> FizzBuzzNumbers { get; set; }
        void GetFizzBuzzNumbers();
    } 
}
