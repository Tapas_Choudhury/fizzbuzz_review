﻿using Microsoft.Practices.Unity;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using FizzBuzz.App_Start;
using FizzBuzz.BL.Contacts;
using FizzBuzz.BL.Implementation;
using FizzBuzz.Contract;
using FizzBuzz.DAL.Contracts;
using FizzBuzz.DAL.Implementation;
using FizzBuzz.ViewModel;
using FizzBuzz.Models;

namespace FizzBuzz
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Set Dependency Injection using Unity
            IUnityContainer myContainer = new UnityContainer();
            myContainer.RegisterType<IFizzBuzzDAL, FizzBuzzDAL>();
            myContainer.RegisterType<IFizzBuzzBL, FizzBuzzBL>();
            myContainer.RegisterType<IFizzBuzzViewModel, PopulateModel>();
            DependencyResolver.SetResolver(new FizzBuzzDependencyResovler(myContainer));
            
            //Register Maaping using AutoMapper
            MappingConfig.RegisterMap();
        }
    }
}
