﻿using FizzBuzz.BL.Contacts;
using FizzBuzz.Contract;

namespace FizzBuzz.Models
{
    public class CommandInvoker
    {
        
        public void Invoke(IFizzBuzzBL cmdObj)
        {
            cmdObj.GetFizzBuzzNumbers();
        }
        public void Invoke(IFizzBuzzViewModel cmdObj)
        {
            cmdObj.GetFizzBuzzNumbers();
        }
    }
}