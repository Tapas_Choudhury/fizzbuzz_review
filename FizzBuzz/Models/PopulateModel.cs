﻿using FizzBuzz.BL.Contacts;
using FizzBuzz.Contract;
using FizzBuzz.DAL.Domain;
using FizzBuzz.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FizzBuzz.Models
{
    public class PopulateModel : IFizzBuzzViewModel
    {
        
        public int MaxNumber { get; set; }
        IFizzBuzzBL FizzBuzzBL;
        public FizzBuzzViewModel Model { get; set; }
        public PopulateModel(IFizzBuzzBL fizzBuzzBL)
        {
            Model = new FizzBuzzViewModel();
            Model.FizzBuzzNumbers = new List<FizzBuzzResultDTO>();           
            FizzBuzzBL = fizzBuzzBL;
        }

        public void GetFizzBuzzNumbers()
        {
            FizzBuzzBL.MaxNumber = MaxNumber;           
            CommandInvoker InvokeObj = new CommandInvoker();
            InvokeObj.Invoke(FizzBuzzBL);
            Model.FizzBuzzNumbers = AutoMapper.Mapper.Map<List<FizzBuzzDomain>, List<FizzBuzzResultDTO>>(FizzBuzzBL.FizzBuzzNumbers, Model.FizzBuzzNumbers);
            Model.MaxNumber = FizzBuzzBL.MaxNumber;
        }

        
    }
}