﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using FizzBuzz.BL.Contacts;
using FizzBuzz.Contract;
using FizzBuzz.DAL.Domain;
using FizzBuzz.Models;

namespace FizzBuzz.ViewModel
{
    public class FizzBuzzViewModel
    {
        [Required]
        [RegularExpression(@"^[1-9]\d{0,2}$", ErrorMessage = "Please enter a valid maximum no.:1-999")]
        [DisplayAttribute(Name="Number")]
        public int MaxNumber { get; set; }
        public string DisplayNumber { get; set; }
        public List<FizzBuzzResultDTO> FizzBuzzNumbers { get; set; }        
    }
}