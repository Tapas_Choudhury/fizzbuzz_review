﻿namespace FizzBuzz.ViewModel
{
    public class FizzBuzzResultDTO
    {
        public string Number { get; set; }
        public string DisplayText { get; set; }
        public string Color { get; set; }
    }
}