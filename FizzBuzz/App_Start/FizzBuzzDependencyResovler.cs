﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

using Microsoft.Practices.Unity;

namespace FizzBuzz.App_Start
{
    public class FizzBuzzDependencyResovler : System.Web.Mvc.IDependencyResolver
    {
        private IUnityContainer container;

        private IDependencyResolver resolver;

        public FizzBuzzDependencyResovler(IUnityContainer container)
        {
            this.container = container;
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return this.container.Resolve(serviceType);
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return this.container.ResolveAll(serviceType);
            }
            catch
            {
                return null;
            }
        }
    }
}