﻿using FizzBuzz.DAL.Domain;
using FizzBuzz.ViewModel;

namespace FizzBuzz.App_Start
{
    public static class MappingConfig
    {
        public static void RegisterMap()
        {
            AutoMapper.Mapper.Initialize(Config =>
            {
                Config.CreateMap<FizzBuzzDomain, FizzBuzzResultDTO>().ReverseMap();

                //.ForMember(dest => dest.Number,
                //opt => opt.MapFrom(src => src.Number))
                //.ForMember(dest => dest.DisplayText, opt => opt.MapFrom(src => src.DisplayText));

            });


        }
    }
}