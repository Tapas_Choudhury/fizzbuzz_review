﻿using System;
using System.Web.Mvc;

using FizzBuzz.Contract;
using FizzBuzz.Models;
using FizzBuzz.ViewModel;

namespace FizzBuzz.Controllers
{
    public class FizzBuzzController : Controller
    {
        public IFizzBuzzViewModel FizzBuzzModel { get; set; }

        public FizzBuzzController(IFizzBuzzViewModel fizzBuzzModel)
        {
            FizzBuzzModel = fizzBuzzModel;
        }

        [HttpGet]
        public ActionResult FizzBuzz()
        {            
            return View("FizzBuzz");
        }

        [HttpPost]
        public ActionResult FizzBuzzResults(string maxNumber)
        {
            if (ModelState.IsValid)
            {
               
                FizzBuzzModel.MaxNumber = Int32.Parse(maxNumber);
                CommandInvoker command = new CommandInvoker();
                command.Invoke(FizzBuzzModel);
                //FizzBuzzModel.GetFizzBuzzNumbers();

                TempData["myModel"] = ((PopulateModel)FizzBuzzModel).Model;
                return RedirectToAction("ShowFizzBuzz", "FizzBuzz");
            }
            else
            {
                return View("FizzBuzz", FizzBuzzModel);
            }
        }
        [HttpGet]
        public ActionResult ShowFizzBuzz()
        {

            return View("FizzBuzz", TempData["myModel"] as FizzBuzzViewModel);
        }
 

    }
}