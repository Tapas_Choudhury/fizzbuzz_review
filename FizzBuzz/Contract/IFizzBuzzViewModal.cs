﻿using System.Collections.Generic;

using FizzBuzz.ViewModel;

namespace FizzBuzz.Contract
{
    public interface IFizzBuzzViewModel
    {
        int MaxNumber { get; set; }
        //List<FizzBuzzResultDTO> FizzBuzzNumbers { get; set; }
        //FizzBuzzViewModel ViewModel { get; set; }
        
        void GetFizzBuzzNumbers();
    }
}
