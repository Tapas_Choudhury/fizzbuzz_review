﻿using System;
using FizzBuzz.DAL.Contracts;

namespace FizzBuzz.DAL.Implementation
{
    /// <summary>
    /// Considerring this as third party service/Data Access Layer. Will be mocked in the UT
    /// </summary>
    public class FizzBuzzDAL : IFizzBuzzDAL
    {
        public virtual string FizzTemplate { get; set; }
        public virtual string BuzzTemplate { get; set; }
        public virtual string WuzzTemplate { get; set; }
        public virtual string WizzTemplate { get; set; }

        public FizzBuzzDAL()
        {
            FizzTemplate = "Fizz";
            BuzzTemplate = "Buzz";
            WizzTemplate = "Wizz";
            WuzzTemplate = "Wuzz";
        }
    }
}
