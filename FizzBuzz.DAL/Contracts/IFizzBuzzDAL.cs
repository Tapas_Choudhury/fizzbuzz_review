﻿namespace FizzBuzz.DAL.Contracts
{
    public interface IFizzBuzzDAL
    {
        string FizzTemplate { get; set; }
        string BuzzTemplate { get; set; }
    }
}
