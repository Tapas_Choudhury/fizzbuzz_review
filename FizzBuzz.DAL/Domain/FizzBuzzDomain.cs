﻿namespace FizzBuzz.DAL.Domain
{
    public class FizzBuzzDomain
    {
        public string Number { get; set; }
        public string DisplayText { get; set; }
        public string Color { get; set; }
    }
}
