﻿using FizzBuzz.BL.Implementation;
using FizzBuzz.DAL.Implementation;
using Moq;
using NUnit.Framework;
using System.Linq;



namespace FizzBuzz.BL.Tests.Implementation
{
    [TestFixture]
    public class FizzBuzzBLTest
    {
        [Test]
        public void Match_Number_Count()
        {
            //Arrange
            FizzBuzzBL fizzBuzzBL = new FizzBuzzBL(new FizzBuzzDAL());


            //Act
            fizzBuzzBL.MaxNumber = 4;
            fizzBuzzBL.GetFizzBuzzNumbers();

            //Assert
            NUnit.Framework.Assert.AreEqual(fizzBuzzBL.MaxNumber, fizzBuzzBL.FizzBuzzNumbers.Count);
        }

        [Test]
        public void Pass5AndRestuns12Fizz4Buzz()
        {
            //Arrange
            FizzBuzzBL fizzBuzzBL = new FizzBuzzBL(new FizzBuzzDAL());

            //Act
            fizzBuzzBL.MaxNumber = 5;
            fizzBuzzBL.GetFizzBuzzNumbers();

            //Assert
            NUnit.Framework.Assert.AreEqual("12Fizz4Buzz", string.Concat(fizzBuzzBL.FizzBuzzNumbers.Select(q => q.DisplayText)));
        }



    }
}
