﻿using FizzBuzz.DAL.Domain;
using FizzBuzz.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Tests
{
    public static class MappingConfig
    {
        public static void RegisterMap()
        {
            AutoMapper.Mapper.Initialize(Config =>
            {
                Config.CreateMap<FizzBuzzDomain, FizzBuzzResultDTO>().ReverseMap();

                //.ForMember(dest => dest.Number,
                //opt => opt.MapFrom(src => src.Number))
                //.ForMember(dest => dest.DisplayText, opt => opt.MapFrom(src => src.DisplayText));

            });


        }
    }
}
