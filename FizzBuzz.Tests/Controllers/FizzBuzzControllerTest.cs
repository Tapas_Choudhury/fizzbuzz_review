﻿using FizzBuzz.BL.Implementation;
using FizzBuzz.Contract;
using FizzBuzz.Controllers;
using FizzBuzz.DAL.Implementation;
using FizzBuzz.ViewModel;
using Moq;
using NUnit.Framework;
using System.Web.Mvc;
using System.Linq;
using FizzBuzz.Models;



namespace FizzBuzz.Tests.Controllers
{   
    [TestFixture]
    public class FizzBuzzControllerTest
    {
        public FizzBuzzControllerTest()
        {
            MappingConfig.RegisterMap();
        }

        [Test]
        public void Match_View_Name()
        {
            //Arrange
            var mock = new Mock<FizzBuzzDAL>();
            mock.Setup(q => q.FizzTemplate).Returns("Fizz");
            mock.Setup(q => q.BuzzTemplate).Returns("Buzz");
            var controller = new FizzBuzzController(new PopulateModel(new FizzBuzzBL(mock.Object)));

            //Act
            ViewResult result = controller.FizzBuzz() as ViewResult;


            //Assert
            Assert.AreEqual("FizzBuzz", result.ViewName);
        }

        [Test]
        public void DivisibleBy3ReturnsFizz()
        {
            //Arrange
            var mock = new Mock<FizzBuzzDAL>();
            mock.Setup(q => q.FizzTemplate).Returns("Fizz");
            mock.Setup(q => q.BuzzTemplate).Returns("Buzz");
            var controller = new FizzBuzzController(new PopulateModel(new FizzBuzzBL(mock.Object)));

            //Act
            controller.FizzBuzzResults("4");
            ViewResult result = controller.ShowFizzBuzz() as ViewResult;
            var model = result.Model as FizzBuzzViewModel;

            //Assert
            Assert.AreEqual("12Fizz4", string.Concat(model.FizzBuzzNumbers.Select(q => q.DisplayText)));
        }

        [Test]
        public void DivisibleBy5ReturnsBuzz()
        {
            //Arrange
            var mock = new Mock<FizzBuzzDAL>();
            mock.Setup(q => q.FizzTemplate).Returns("Fizz");
            mock.Setup(q => q.BuzzTemplate).Returns("Buzz");
            var controller = new FizzBuzzController(new PopulateModel(new FizzBuzzBL(mock.Object)));

            //Act
            controller.FizzBuzzResults("5");
            ViewResult result = controller.ShowFizzBuzz() as ViewResult;
            var model = result.Model as FizzBuzzViewModel;

            //Assert
            Assert.AreEqual("12Fizz4Buzz", string.Concat(model.FizzBuzzNumbers.Select(q => q.DisplayText)));
        }

        [Test]
        public void DivisibleByBoth3and5ReturnsFizzBuzz()
        {
            //Arrange
            var mock = new Mock<FizzBuzzDAL>();
            mock.Setup(q => q.FizzTemplate).Returns("Fizz");
            mock.Setup(q => q.BuzzTemplate).Returns("Buzz");
            var controller = new FizzBuzzController(new PopulateModel(new FizzBuzzBL(mock.Object)));

            //Act
            controller.FizzBuzzResults("15");
            ViewResult result = controller.ShowFizzBuzz() as ViewResult;
            var model = result.Model as FizzBuzzViewModel;

            //Assert
            Assert.AreEqual("12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz", string.Concat(model.FizzBuzzNumbers.Select(q => q.DisplayText)));
        }

        [Test]
        public void DivisibleBy3ReturnsWizzOnWednesday()
        {
            //Arrange
            var mock = new Mock<FizzBuzzDAL>();
            mock.Setup(q => q.FizzTemplate).Returns("Fizz");
            mock.Setup(q => q.BuzzTemplate).Returns("Buzz");
            var controller = new FizzBuzzController(new PopulateModel(new FizzBuzzBL(mock.Object)));

            //Act
            controller.FizzBuzzResults("4");
            ViewResult result = controller.ShowFizzBuzz() as ViewResult;
            var model = result.Model as FizzBuzzViewModel;

            //Assert
            if(System.DateTime.Now.DayOfWeek.Equals(System.DayOfWeek.Wednesday))
                Assert.AreEqual("12Wizz4", string.Concat(model.FizzBuzzNumbers.Select(q => q.DisplayText)));
            else
                Assert.AreEqual("12Fizz4", string.Concat(model.FizzBuzzNumbers.Select(q => q.DisplayText)));
        }
    }
}
